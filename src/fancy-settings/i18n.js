// SAMPLE
this.i18n = {
    "settings": {
        "en": "Settings",
        "de": "Optionen",
        "fr": "Paramètres"
    },
    "Poche configuration": {
        "en": "Poche configuration",
        "fr": "Configuration de Poche"
    },
    "Server information": {
        "en": "Server Information",
        "fr": "Information sur le serveur"
    },
    "search": {
        "en": "Search",
        "de": "Suche"
    },
    "nothing-found": {
        "en": "No matches were found.",
        "de": "Keine Übereinstimmungen gefunden."
    },
    
    
    
    "information": {
        "en": "Information",
        "de": "Information"
    },
    "login": {
        "en": "Login",
        "de": "Anmeldung"
    },
    "username": {
        "en": "Username:",
        "de": "Benutzername:"
    },
    "password": {
        "en": "Password:",
        "de": "Passwort:"
    },
    "x-url": {
        "en": "http://www.domain.com/poche/"
    },
    "x-characters-pw": {
        "en": "10 - 18 characters",
        "de": "10 - 18 Zeichen"
    },
    "Base URL": {
        "en": "Base URL",
        "fr": "URL racine"
    },
    "description": {
        "en": "Type here the base URL of your Poche.<br>\
            It should look something like <pre>http://www.domain.com/poche/</pre>.",
        "fr": "Saisissez ici l'URL racine de votre Poche.<br>\
            Elle devrait ressembler à :<pre>http://www.domain.com/poche/</pre>"
    },
    "logout": {
        "en": "Logout",
        "de": "Abmeldung"
    },
    "enable": {
        "en": "Enable",
        "de": "Aktivieren"
    },
    "disconnect": {
        "en": "Disconnect:",
        "de": "Trennen:"
    }
};
